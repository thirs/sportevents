package com.example.teo.sportevents;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;

import com.example.teo.sportevents.home.SportEventsFragment;

/**
 * Created by thirs on 17.10.2015..
 */
public class HostActivity extends FragmentActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.host_activity);
        init();
    }

    protected void init() {
        FragmentManager fragmentManager = getSupportFragmentManager();
        Fragment fragment = (Fragment) fragmentManager.findFragmentByTag(SportEventsFragment.TAG);
        if (fragment == null) {
            fragment = SportEventsFragment.newInstance();
            addFragment(fragment, SportEventsFragment.TAG);
        }else{
            replaceFragmet(fragment, SportEventsFragment.TAG);
        }
    }

    protected void addFragment(Fragment fragment, String TAG) {
        FragmentManager fm = getSupportFragmentManager();
        fm.beginTransaction().add(R.id.fragment_container, fragment, TAG).addToBackStack(TAG).commit();
    }

    protected void replaceFragmet(Fragment fragment, String TAG) {
        FragmentManager fm = getSupportFragmentManager();
        fm.beginTransaction().replace(R.id.fragment_container, fragment, TAG).addToBackStack(TAG).commit();
    }
}
