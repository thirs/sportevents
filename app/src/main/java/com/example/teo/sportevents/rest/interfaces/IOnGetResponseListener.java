package com.example.teo.sportevents.rest.interfaces;

import retrofit.Response;

public interface IOnGetResponseListener {
	public void onGetResponse(Response response, int requestId);
}
