package com.example.teo.sportevents;

/**
 * Created by Teo on 5.7.2016..
 *
 * Class containing static variables used for basic app configuration
 */
public class Filter {

    public static final boolean	isDebuggingMode			= true;

    public static final String	resApi_Url				= "http://api.sport1.de/";

    public static final int CONNECT_TIMEOUT             = 5; //in s
    public static final int READ_TIMEOUT                = 20; //in s
}
