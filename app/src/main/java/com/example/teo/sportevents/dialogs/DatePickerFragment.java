package com.example.teo.sportevents.dialogs;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.widget.DatePicker;

import java.util.Calendar;

/**
 * Created by Teo on 6.7.2016..
 */
public class DatePickerFragment extends DialogFragment {
    private DatePickerDialog.OnDateSetListener mListener;
    private int year, month, day;


    public void setDateSetListener(DatePickerDialog.OnDateSetListener listener){
        mListener = listener;
    }

    public void setCurentDate(int year, int month, int day){
        this.year = year;
        this.month = month;
        this.day = day;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        int y;
        int m;
        int d;
        if(year > 0 && month > 0 && day >0){
            y = year;
            m = month;
            d = day;
        }else{
            // Use the current date as the default date in the picker
            final Calendar c = Calendar.getInstance();
            y = c.get(Calendar.YEAR);
            m = c.get(Calendar.MONTH);
            d = c.get(Calendar.DAY_OF_MONTH);
        }

        // Create a new instance of DatePickerDialog and return it
        return new DatePickerDialog(getActivity(), mListener, y, m, d);
    }
}
