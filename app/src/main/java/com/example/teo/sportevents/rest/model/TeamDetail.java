package com.example.teo.sportevents.rest.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Teo on 5.7.2016..
 */
public class TeamDetail implements Parcelable {

    @SerializedName("colours")
    private String colours = null;
    @SerializedName("foundation")
    private String foundation = null;
    @SerializedName("fullname")
    private String fullname = null;
    @SerializedName("id")
    private String id = null;
    @SerializedName("members")
    private String members = null;

    public String getColours() {
        return colours;
    }

    public void setColours(String colours) {
        this.colours = colours;
    }

    public String getFoundation() {
        return foundation;
    }

    public void setFoundation(String foundation) {
        this.foundation = foundation;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getMembers() {
        return members;
    }

    public void setMembers(String members) {
        this.members = members;
    }

    protected TeamDetail(Parcel in) {
        colours = in.readString();
        foundation = in.readString();
        fullname = in.readString();
        id = in.readString();
        members = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(colours);
        dest.writeString(foundation);
        dest.writeString(fullname);
        dest.writeString(id);
        dest.writeString(members);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<TeamDetail> CREATOR = new Parcelable.Creator<TeamDetail>() {
        @Override
        public TeamDetail createFromParcel(Parcel in) {
            return new TeamDetail(in);
        }

        @Override
        public TeamDetail[] newArray(int size) {
            return new TeamDetail[size];
        }
    };
}