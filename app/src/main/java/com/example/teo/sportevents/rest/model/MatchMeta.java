package com.example.teo.sportevents.rest.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Teo on 5.7.2016..
 */
public class MatchMeta implements Parcelable {
    @SerializedName("content")
    private String content = null;
    @SerializedName("kind")
    private String kind = null;

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getKind() {
        return kind;
    }

    public void setKind(String kind) {
        this.kind = kind;
    }

    protected MatchMeta(Parcel in) {
        content = in.readString();
        kind = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(content);
        dest.writeString(kind);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<MatchMeta> CREATOR = new Parcelable.Creator<MatchMeta>() {
        @Override
        public MatchMeta createFromParcel(Parcel in) {
            return new MatchMeta(in);
        }

        @Override
        public MatchMeta[] newArray(int size) {
            return new MatchMeta[size];
        }
    };
}
