package com.example.teo.sportevents.rest.interfaces;

import retrofit.Response;

public interface IOnFailedResponseListener {
	public void onFailedResponse(Throwable throwable, int requestId);
}
