package com.example.teo.sportevents.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import java.util.List;

import com.example.teo.sportevents.Filter;
import com.example.teo.sportevents.R;
import com.example.teo.sportevents.rest.interfaces.ISportEventType;
import com.example.teo.sportevents.rest.model.SportEvents;


/**
 * Created by Teo on 6.7.2016..
 */
public class EventCardsAdapter extends RecyclerView.Adapter<EventCardsAdapter.ViewHolder> {
    private static final String TAG = "EventCardsAdapter";
    private List<SportEvents> mDataset;
    private Context context;

    public EventCardsAdapter(List<SportEvents> myDataset) {
        mDataset = myDataset;
    }

    public void updateDataSet(List<SportEvents> dataSet){
        mDataset = dataSet;
    }

    @Override
    public EventCardsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        context = parent.getContext();
        View v = LayoutInflater.from(context).inflate(R.layout.event_card, parent, false);
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        SportEvents event = mDataset.get(position);
        if(Filter.isDebuggingMode){
            Log.i(TAG, "Populating: "+event.getName() + "; ID:" +event.getId());
        }
        ISportEventType.SportEventType type = ((ISportEventType)event).getSportEventType(event.getSportId());
        holder.eventTitle.setText(event.getName() != null ? event.getName() : context.getResources().getString(R.string.unknown));
        String eventTime = String.format(context.getResources().getString(R.string.event_card_time), event.getMatch() != null && event.getMatch().getMatchTime() != null ? event.getMatch().getMatchTime() : context.getResources().getString(R.string.unknown));
        holder.eventTime.setText(eventTime);
        if(event.getMatch().getVenue() != null && event.getMatch().getVenue().getName() != null && event.getMatch().getVenue().getTown() != null && event.getMatch().getVenue().getCountry() != null){
            StringBuilder eventLocaiton = new StringBuilder();
            eventLocaiton.append(event.getMatch().getVenue().getName());
            eventLocaiton.append(", ");
            eventLocaiton.append(event.getMatch().getVenue().getTown().getName());
            eventLocaiton.append("(");
            eventLocaiton.append(event.getMatch().getVenue().getCountry().getName());
            eventLocaiton.append(")");
            holder.eventSubtitle.setVisibility(View.VISIBLE);
            holder.eventSubtitle.setText(eventLocaiton);
        }

        if((type == ISportEventType.SportEventType.FOOTBALL)
                ||(type == ISportEventType.SportEventType.BASEBALL)
                ||(type == ISportEventType.SportEventType.TENNIS)){
            holder.eventHomeTeam.setVisibility(View.VISIBLE);
            holder.eventAwayTeam.setVisibility(View.VISIBLE);
            holder.eventAwayTeam.setText(event.getMatch() != null && event.getMatch().getAway() != null && event.getMatch().getAway().getName() != null ? event.getMatch().getAway().getName() : "UNKNOWN");
            holder.eventHomeTeam.setText(event.getMatch() != null && event.getMatch().getHome() != null && event.getMatch().getHome().getName() != null ? event.getMatch().getHome().getName() : "UNKNOWN");
        }else if ((type == ISportEventType.SportEventType.CYCLING)
                || (type == ISportEventType.SportEventType.INVALID)
                || (type == ISportEventType.SportEventType.FORMULA1)){
            holder.eventHomeTeam.setVisibility(View.GONE);
            holder.eventAwayTeam.setVisibility(View.GONE);
        }

    }

    @Override
    public int getItemCount() {
        return mDataset.size();
    }


    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView eventTitle;
        public TextView eventSubtitle;
        public TextView eventTime;
        public TextView eventHomeTeam;
        public TextView eventAwayTeam;
        public View teamSection;
        public ViewHolder(View v) {
            super(v);
            eventTitle = (TextView) v.findViewById(R.id.event_title);
            eventSubtitle = (TextView) v.findViewById(R.id.event_subtitle);
            eventTime = (TextView) v.findViewById(R.id.event_time);
            eventHomeTeam = (TextView) v.findViewById(R.id.home_name);
            eventAwayTeam = (TextView) v.findViewById(R.id.away_name);
            teamSection = v.findViewById(R.id.team_section);
        }
    }
}
