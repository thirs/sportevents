package com.example.teo.sportevents.rest.services;

import com.example.teo.sportevents.rest.model.SportEvents;

import java.util.List;

import retrofit.Call;
import retrofit.http.GET;
import retrofit.http.Path;

public interface ISport1ApiService {
	public static final int STATUS_CODE_GENERAL_OK                  = 200;

	//REST METHODS
	public static final String method_matchesByDate    				= "/matches-by-date-shortlist";


	//	Matches
	@GET("api/sports/" + method_matchesByDate+ "/da{date}")
	Call<List<SportEvents>> getMatchesByDate(@Path("date") String date);
}