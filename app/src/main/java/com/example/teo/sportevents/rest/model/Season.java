package com.example.teo.sportevents.rest.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Teo on 5.7.2016..
 */
public class Season implements Parcelable {

    @SerializedName("competition_id")
    private String competitionId = null;
    @SerializedName("end")
    private String end = null;
    @SerializedName("name")
    private String name = null;
    @SerializedName("has_tables")
    private String hasTables = null;
    @SerializedName("round")
    private List<Round> roundList = null;
    @SerializedName("start")
    private String start = null;
    @SerializedName("current_round_id")
    private String currentRoundId = null;
    @SerializedName("current_group_matchday")
    private String currentGroupMatchday = null;
    @SerializedName("id")
    private String id = null;

    public String getCompetitionId() {
        return competitionId;
    }

    public void setCompetitionId(String competitionId) {
        this.competitionId = competitionId;
    }

    public String getEnd() {
        return end;
    }

    public void setEnd(String end) {
        this.end = end;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getHasTables() {
        return hasTables;
    }

    public void setHasTables(String hasTables) {
        this.hasTables = hasTables;
    }

    public List<Round> getRoundList() {
        return roundList;
    }

    public void setRoundList(List<Round> roundList) {
        this.roundList = roundList;
    }

    public String getStart() {
        return start;
    }

    public void setStart(String start) {
        this.start = start;
    }

    public String getCurrentRoundId() {
        return currentRoundId;
    }

    public void setCurrentRoundId(String currentRoundId) {
        this.currentRoundId = currentRoundId;
    }

    public String getCurrentGroupMatchday() {
        return currentGroupMatchday;
    }

    public void setCurrentGroupMatchday(String currentGroupMatchday) {
        this.currentGroupMatchday = currentGroupMatchday;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    protected Season(Parcel in) {
        competitionId = in.readString();
        end = in.readString();
        name = in.readString();
        hasTables = in.readString();
        if (in.readByte() == 0x01) {
            roundList = new ArrayList<Round>();
            in.readList(roundList, Round.class.getClassLoader());
        } else {
            roundList = null;
        }
        start = in.readString();
        currentRoundId = in.readString();
        currentGroupMatchday = in.readString();
        id = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(competitionId);
        dest.writeString(end);
        dest.writeString(name);
        dest.writeString(hasTables);
        if (roundList == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeList(roundList);
        }
        dest.writeString(start);
        dest.writeString(currentRoundId);
        dest.writeString(currentGroupMatchday);
        dest.writeString(id);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<Season> CREATOR = new Parcelable.Creator<Season>() {
        @Override
        public Season createFromParcel(Parcel in) {
            return new Season(in);
        }

        @Override
        public Season[] newArray(int size) {
            return new Season[size];
        }
    };
}