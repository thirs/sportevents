package com.example.teo.sportevents.rest.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Teo on 5.7.2016..
 */
public class Away implements Parcelable {
    @SerializedName("name")
    private String name = null;
    @SerializedName("gender")
    private String gender = null;
    @SerializedName("team_detail")
    private TeamDetail teamDetail = null;
    @SerializedName("venue")
    private Venue venue = null;
    @SerializedName("microname")
    private String microname = null;
    @SerializedName("country")
    private Country country = null;
    @SerializedName("show_team")
    private String showTeam = null;
    @SerializedName("shortname")
    private String shortname = null;
    @SerializedName("type")
    private String type = null;
    @SerializedName("id")
    private String id = null;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public TeamDetail getTeamDetail() {
        return teamDetail;
    }

    public void setTeamDetail(TeamDetail teamDetail) {
        this.teamDetail = teamDetail;
    }

    public Venue getVenue() {
        return venue;
    }

    public void setVenue(Venue venue) {
        this.venue = venue;
    }

    public String getMicroname() {
        return microname;
    }

    public void setMicroname(String microname) {
        this.microname = microname;
    }

    public Country getCountry() {
        return country;
    }

    public void setCountry(Country country) {
        this.country = country;
    }

    public String getShowTeam() {
        return showTeam;
    }

    public void setShowTeam(String showTeam) {
        this.showTeam = showTeam;
    }

    public String getShortname() {
        return shortname;
    }

    public void setShortname(String shortname) {
        this.shortname = shortname;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    protected Away(Parcel in) {
        name = in.readString();
        gender = in.readString();
        teamDetail = (TeamDetail) in.readValue(TeamDetail.class.getClassLoader());
        venue = (Venue) in.readValue(Venue.class.getClassLoader());
        microname = in.readString();
        country = (Country) in.readValue(Country.class.getClassLoader());
        showTeam = in.readString();
        shortname = in.readString();
        type = in.readString();
        id = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
        dest.writeString(gender);
        dest.writeValue(teamDetail);
        dest.writeValue(venue);
        dest.writeString(microname);
        dest.writeValue(country);
        dest.writeString(showTeam);
        dest.writeString(shortname);
        dest.writeString(type);
        dest.writeString(id);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<Away> CREATOR = new Parcelable.Creator<Away>() {
        @Override
        public Away createFromParcel(Parcel in) {
            return new Away(in);
        }

        @Override
        public Away[] newArray(int size) {
            return new Away[size];
        }
    };
}