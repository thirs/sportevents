package com.example.teo.sportevents.rest.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.example.teo.sportevents.rest.interfaces.ISportEventType;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Teo on 5.7.2016..
 */
public class SportEvents extends ISportEventType implements Parcelable {

    @SerializedName("current_matchday")
    private String currentMatchday = null;
    @SerializedName("name")
    private String name = null;
    @SerializedName("gender")
    private String gender = null;
    @SerializedName("age_id")
    private String ageId = null;
    @SerializedName("current_round_id")
    private String currentRoundId = null;
    @SerializedName("season")
    private List<Season> seasonList = null;
    @SerializedName("policy")
    private EventPolicy policy = null;
    @SerializedName("current_season_id")
    private String currentSeasonId = null;
    @SerializedName("type")
    private String type = null;
    @SerializedName("id")
    private String id = null;
    @SerializedName("sport_id")
    private String sportId = null;

    public String getCurrentMatchday() {
        return currentMatchday;
    }

    public void setCurrentMatchday(String currentMatchday) {
        this.currentMatchday = currentMatchday;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getAgeId() {
        return ageId;
    }

    public void setAgeId(String ageId) {
        this.ageId = ageId;
    }

    public String getCurrentRoundId() {
        return currentRoundId;
    }

    public void setCurrentRoundId(String currentRoundId) {
        this.currentRoundId = currentRoundId;
    }

    public List<Season> getSeasonList() {
        return seasonList;
    }

    public void setSeasonList(List<Season> seasonList) {
        this.seasonList = seasonList;
    }

    public EventPolicy getPolicy() {
        return policy;
    }

    public void setPolicy(EventPolicy policy) {
        this.policy = policy;
    }

    public String getCurrentSeasonId() {
        return currentSeasonId;
    }

    public void setCurrentSeasonId(String currentSeasonId) {
        this.currentSeasonId = currentSeasonId;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSportId() {
        return sportId;
    }

    public void setSportId(String sportId) {
        this.sportId = sportId;
    }

    protected SportEvents(Parcel in) {
        currentMatchday = in.readString();
        name = in.readString();
        gender = in.readString();
        ageId = in.readString();
        currentRoundId = in.readString();
        if (in.readByte() == 0x01) {
            seasonList = new ArrayList<Season>();
            in.readList(seasonList, Season.class.getClassLoader());
        } else {
            seasonList = null;
        }
        policy = (EventPolicy) in.readValue(EventPolicy.class.getClassLoader());
        currentSeasonId = in.readString();
        type = in.readString();
        id = in.readString();
        sportId = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(currentMatchday);
        dest.writeString(name);
        dest.writeString(gender);
        dest.writeString(ageId);
        dest.writeString(currentRoundId);
        if (seasonList == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeList(seasonList);
        }
        dest.writeValue(policy);
        dest.writeString(currentSeasonId);
        dest.writeString(type);
        dest.writeString(id);
        dest.writeString(sportId);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<SportEvents> CREATOR = new Parcelable.Creator<SportEvents>() {
        @Override
        public SportEvents createFromParcel(Parcel in) {
            return new SportEvents(in);
        }

        @Override
        public SportEvents[] newArray(int size) {
            return new SportEvents[size];
        }
    };

    // TODO: 6.7.2016. instead of interface work with abstract class and move helper methods
    //Helper Methods
    public Season getSeason(){
        Season season = null;
        if(getSeasonList() != null && getSeasonList().size() > 0){
//            for(Season s : getSeasonList()){
//                if(s.getId().equals(getCurrentSeasonId())){
//                    season = s;
//                }
//            }
            if(season == null){
                season = getSeasonList().get(0);
            }
        }
        return season;
    }

    public Round getRound(){
        Round round = null;
        Season s = getSeason();
        if(s != null && s.getRoundList() != null && s.getRoundList().size() > 0){
//            for (Round r : s.getRoundList()){
//                if(r.getId().equals(getCurrentRoundId())){
//                    round = r;
//                }
//            }
            if(round == null){
                round = s.getRoundList().get(0);
            }
        }
        return round;
    }

    public Match getMatch(){
        Match match = null;
        Round r = getRound();
        if(r != null && r.getMatchList() != null && r.getMatchList().size() > 0){
//            for (Match m : r.getMatchList()){
//                if(m.getMatchday().equals(r.getCurrentMatchday())){
//                    match = m;
//                }else if(m.getGroupMatchday().equals(r.getCurrentMatchday())){
//                    match = m;
//                }
//            }
            if(match == null){
                match = r.getMatchList().get(0);
            }
        }
        return match;
    }
}