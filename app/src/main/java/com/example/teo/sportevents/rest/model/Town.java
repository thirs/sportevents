package com.example.teo.sportevents.rest.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Teo on 5.7.2016..
 */
public class Town implements Parcelable {
    @SerializedName("postcode_start")
    private String postcodeStart = null;
    @SerializedName("postcode_end")
    private String postcodeEnd = null;
    @SerializedName("id")
    private String id = null;
    @SerializedName("name")
    private String name = null;

    public String getPostcodeStart() {
        return postcodeStart;
    }

    public void setPostcodeStart(String postcodeStart) {
        this.postcodeStart = postcodeStart;
    }

    public String getPostcodeEnd() {
        return postcodeEnd;
    }

    public void setPostcodeEnd(String postcodeEnd) {
        this.postcodeEnd = postcodeEnd;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    protected Town(Parcel in) {
        postcodeStart = in.readString();
        postcodeEnd = in.readString();
        id = in.readString();
        name = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(postcodeStart);
        dest.writeString(postcodeEnd);
        dest.writeString(id);
        dest.writeString(name);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<Town> CREATOR = new Parcelable.Creator<Town>() {
        @Override
        public Town createFromParcel(Parcel in) {
            return new Town(in);
        }

        @Override
        public Town[] newArray(int size) {
            return new Town[size];
        }
    };
}
