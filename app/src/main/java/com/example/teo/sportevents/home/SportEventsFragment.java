package com.example.teo.sportevents.home;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.TextView;
import android.widget.Toast;

import com.example.teo.sportevents.R;
import com.example.teo.sportevents.Utils.Util;
import com.example.teo.sportevents.adapters.EventCardsAdapter;
import com.example.teo.sportevents.dialogs.DatePickerFragment;
import com.example.teo.sportevents.rest.controlers.RestControler;
import com.example.teo.sportevents.rest.interfaces.IOnGetResponseListener;
import com.example.teo.sportevents.rest.model.SportEvents;
import com.example.teo.sportevents.rest.services.ISport1ApiService;

import java.util.Calendar;
import java.util.List;

import retrofit.Response;

/**
 * Created by Teo on 6.7.2016..
 */
public class SportEventsFragment extends Fragment implements IOnGetResponseListener, DatePickerDialog.OnDateSetListener  {
    public static final String TAG = "SportEventsFragment";

    private static final int GET_MATCHES_BY_DATE = 31;
    private static final String STATE_DATE = "statedate";

    private RecyclerView mRecyclerView;
    private EventCardsAdapter mAdapter;
    private TextView datePickingTextView;
    private String selectedDate;
    private TextView listEmptyText;

    public static SportEventsFragment newInstance() {
        SportEventsFragment fragment = new SportEventsFragment();
        Bundle args = new Bundle();
        return fragment;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (savedInstanceState != null) {
            if(savedInstanceState.containsKey(STATE_DATE)){
                selectedDate = savedInstanceState.getString(STATE_DATE);
                presentLayout();
            }
        }
        fetchResults(selectedDate);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.sport_events, container, false);
        selectedDate = selectedDate != null ? selectedDate : Util.getCurrentDate();
        initializeLayout(v);
        presentLayout();
        return v;
    }

    private void initializeLayout(View v) {
        datePickingTextView = (TextView)v.findViewById(R.id.event_date);
        mRecyclerView = (RecyclerView) v.findViewById(R.id.sportevents_list);
        listEmptyText = (TextView)v.findViewById(R.id.list_empty);
        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
//        mRecyclerView.setHasFixedSize(true);
        // use a linear layout manager
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(mLayoutManager);
    }

    private void presentLayout() {
        datePickingTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DatePickerFragment datePickerFragment = new DatePickerFragment();
                datePickerFragment.setDateSetListener(SportEventsFragment.this);
                //Preparing Dialog to show currently selected date - not the initial one
                Integer[] parsedDate = Util.parseDate(selectedDate);
                datePickerFragment.setCurentDate(parsedDate[0], parsedDate[1], parsedDate[2]);

                datePickerFragment.show(getFragmentManager(), "datePicker");
            }
        });
        datePickingTextView.setText(selectedDate);
    }

    private void fetchResults(String s) {
        RestControler lc = new RestControler(getActivity());
        lc.setOnGetResponseListener(this);
        lc.getMatchesByDate(s, GET_MATCHES_BY_DATE);
    }

    @Override
    public void onGetResponse(Response response, int requestId) {
        if(response.code() == ISport1ApiService.STATUS_CODE_GENERAL_OK){
            switch(requestId){
                case GET_MATCHES_BY_DATE:
                    if(response.body() instanceof List && !((List)response.body()).isEmpty()){
                        mRecyclerView.setVisibility(View.VISIBLE);
                        listEmptyText.setVisibility(View.GONE);
                        showEvents((List)response.body());
                    }
                    else{
                        mRecyclerView.setVisibility(View.GONE);
                        listEmptyText.setVisibility(View.VISIBLE);
                    }
                    break;
            }
        }else{
            mRecyclerView.setVisibility(View.GONE);
            listEmptyText.setVisibility(View.VISIBLE);
        }
    }

    private void showEvents(List<SportEvents> sportEvents){
        if(mAdapter == null){
            mAdapter = new EventCardsAdapter(sportEvents);
        }
        if(mRecyclerView.getAdapter() == null){
            mRecyclerView.setAdapter(mAdapter);
        }else{
            mAdapter.updateDataSet(sportEvents);
            mAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
        selectedDate = Util.reformatPickedDate(year, monthOfYear, dayOfMonth);
        datePickingTextView.setText(selectedDate);
        fetchResults(selectedDate);
    }



    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString(STATE_DATE,selectedDate);
    }
}
