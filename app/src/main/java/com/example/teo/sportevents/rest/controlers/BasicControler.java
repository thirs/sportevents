package com.example.teo.sportevents.rest.controlers;


import android.app.ProgressDialog;
import android.content.Context;

import com.example.teo.sportevents.Filter;
import com.example.teo.sportevents.R;
import com.example.teo.sportevents.rest.interceptors.HttpLoggingInterceptor;
import com.example.teo.sportevents.rest.interfaces.IOnFailedResponseListener;
import com.example.teo.sportevents.rest.interfaces.IOnGetResponseListener;
import com.example.teo.sportevents.rest.services.ISport1ApiService;
import com.squareup.okhttp.OkHttpClient;
import java.util.concurrent.TimeUnit;
import retrofit.GsonConverterFactory;
import retrofit.Retrofit;

/**
 * Created by thirs on 11.10.2015..
 */
public class BasicControler {
    private static final String LOG_TAG = "BasicControler";

    //request listeners
    protected IOnGetResponseListener onGetResponseListener;
    protected IOnFailedResponseListener onFailedResponseListener;

    protected Context context;
    protected ProgressDialog mProgressDialog;

    public BasicControler(Context context) {
        this.context = context;
        mProgressDialog = new ProgressDialog(context);
    }

    protected ISport1ApiService getRestService(){
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Filter.resApi_Url)
                .addConverterFactory(GsonConverterFactory.create())
                .client(getHTTPClient())
                .build();
        ISport1ApiService service = retrofit.create(ISport1ApiService.class);
        return service;
    }

    protected OkHttpClient getHTTPClient() {
        OkHttpClient okHttpClient = new OkHttpClient();
        okHttpClient.setReadTimeout(Filter.READ_TIMEOUT, TimeUnit.SECONDS);
        okHttpClient.setConnectTimeout(Filter.CONNECT_TIMEOUT, TimeUnit.SECONDS);
        if(Filter.isDebuggingMode){
            HttpLoggingInterceptor interc = new HttpLoggingInterceptor();
            interc.setLevel(HttpLoggingInterceptor.Level.BODY);
            okHttpClient.interceptors().add(interc);
        }
        return okHttpClient;
    }



    //LISTENER GET/SET
    public IOnGetResponseListener getOnGetResponseListener(){
        return onGetResponseListener;
    }

    public void setOnGetResponseListener(IOnGetResponseListener onGetResponseListener){
        this.onGetResponseListener = onGetResponseListener;
    }

    public IOnFailedResponseListener getOnFailedResponseListener() {
        return onFailedResponseListener;
    }

    public void setOnFailedResponseListener(IOnFailedResponseListener onFailedResponseListener) {
        this.onFailedResponseListener = onFailedResponseListener;
    }

    //ProgressBar
    protected void progressBar(boolean show){

        if(show && mProgressDialog != null && !mProgressDialog.isShowing() ){
            mProgressDialog.setIndeterminate(true);
            mProgressDialog.setMessage(context.getResources().getString(R.string.loading_string));
            mProgressDialog.show();
        }else if (show && mProgressDialog == null){
            mProgressDialog = new ProgressDialog(context);
            mProgressDialog.setIndeterminate(true);
            mProgressDialog.setMessage(context.getResources().getString(R.string.loading_string));
            mProgressDialog.show();
        }else if(!show && mProgressDialog != null && mProgressDialog.isShowing()){
            mProgressDialog.dismiss();
        }

    }
}
