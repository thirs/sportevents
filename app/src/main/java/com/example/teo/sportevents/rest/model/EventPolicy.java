package com.example.teo.sportevents.rest.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Teo on 5.7.2016..
 */
public class EventPolicy implements Parcelable {
    @SerializedName("policy_meta")
    private String policyMeta = null;
    @SerializedName("id")
    private String id = null;
    @SerializedName("policy_order")
    private String policyOrder = null;

    public String getPolicyMeta() {
        return policyMeta;
    }

    public void setPolicyMeta(String policyMeta) {
        this.policyMeta = policyMeta;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPolicyOrder() {
        return policyOrder;
    }

    public void setPolicyOrder(String policyOrder) {
        this.policyOrder = policyOrder;
    }

    protected EventPolicy(Parcel in) {
        policyMeta = in.readString();
        id = in.readString();
        policyOrder = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(policyMeta);
        dest.writeString(id);
        dest.writeString(policyOrder);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<EventPolicy> CREATOR = new Parcelable.Creator<EventPolicy>() {
        @Override
        public EventPolicy createFromParcel(Parcel in) {
            return new EventPolicy(in);
        }

        @Override
        public EventPolicy[] newArray(int size) {
            return new EventPolicy[size];
        }
    };
}