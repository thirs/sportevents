package com.example.teo.sportevents.rest.controlers;


import android.app.ProgressDialog;
import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.example.teo.sportevents.Filter;
import com.example.teo.sportevents.rest.model.SportEvents;

import java.util.List;

import retrofit.Callback;
import retrofit.Response;

/**
 * Created by thirs on 11.10.2015..
 */
public class RestControler extends BasicControler {
    private static final String LOG_TAG = "RestControler";


    public RestControler(Context context) {
        super(context);
        this.context = context;
        mProgressDialog = new ProgressDialog(context);
    }


    public void getMatchesByDate(String date, int requestId){
        progressBar(true);
        final int requestIdentifier = requestId;
        getRestService().getMatchesByDate(date).enqueue(new Callback<List<SportEvents>>() {
            @Override
            public void onResponse(Response<List<SportEvents>> response) {
                progressBar(false);
                if (onGetResponseListener != null) {
                    if(Filter.isDebuggingMode){
                        Log.i(LOG_TAG, "getMatchesByDate response - succeeded");
                    }
                    onGetResponseListener.onGetResponse(response, requestIdentifier);
                }
            }

            @Override
            public void onFailure(Throwable t) {
                progressBar(false);
                Toast.makeText(context,t.getMessage(),Toast.LENGTH_LONG).show();
                if (onFailedResponseListener != null) {
                    onFailedResponseListener.onFailedResponse(t, requestIdentifier);
                }
            }
        });
    }

}
