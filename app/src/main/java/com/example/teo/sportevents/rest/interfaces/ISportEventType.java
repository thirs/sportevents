package com.example.teo.sportevents.rest.interfaces;

/**
 * Created by Teo on 6.7.2016..
 */
public abstract class ISportEventType {
    public static final String SPORT_TYPE_FOOTBALL          = "1";
    public static final String SPORT_TYPE_CYCLING           = "138";
    public static final String SPORT_TYPE_TENNIS            = "5";
    public static final String SPORT_TYPE_BASEBALL          = "3";
    public static final String SPORT_TYPE_FORMULA1          = "40";

    public enum SportEventType{
        INVALID,
        FOOTBALL,
        CYCLING,
        TENNIS,
        BASEBALL,
        FORMULA1
    }

    public SportEventType getSportEventType(String sportId) {
        SportEventType type = SportEventType.INVALID;
        if(sportId.equals(ISportEventType.SPORT_TYPE_BASEBALL)){
            type = SportEventType.BASEBALL;
        }else if(sportId.equals(ISportEventType.SPORT_TYPE_CYCLING)){
            type = SportEventType.CYCLING;
        }else if(sportId.equals(ISportEventType.SPORT_TYPE_FOOTBALL)){
            type = SportEventType.FOOTBALL;
        }else if(sportId.equals(ISportEventType.SPORT_TYPE_TENNIS)){
            type = SportEventType.TENNIS;
        }else if(sportId.equals(ISportEventType.SPORT_TYPE_FORMULA1)){
            type = SportEventType.FORMULA1;
        }
        return type;
    }
}
