package com.example.teo.sportevents.rest.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Teo on 5.7.2016..
 */
public class Round implements Parcelable {
    @SerializedName("match_count")
    private String matchCount = null;
    @SerializedName("current_matchday")
    private String currentMatchday = null;
    @SerializedName("name")
    private String name = null;
    @SerializedName("season_id")
    private String seasonId = null;
    @SerializedName("round_order")
    private String roundOrder = null;
    @SerializedName("id")
    private String id = null;
    @SerializedName("match")
    private List<Match> matchList = null;

    public String getMatchCount() {
        return matchCount;
    }

    public void setMatchCount(String matchCount) {
        this.matchCount = matchCount;
    }

    public String getCurrentMatchday() {
        return currentMatchday;
    }

    public void setCurrentMatchday(String currentMatchday) {
        this.currentMatchday = currentMatchday;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSeasonId() {
        return seasonId;
    }

    public void setSeasonId(String seasonId) {
        this.seasonId = seasonId;
    }

    public String getRoundOrder() {
        return roundOrder;
    }

    public void setRoundOrder(String roundOrder) {
        this.roundOrder = roundOrder;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public List<Match> getMatchList() {
        return matchList;
    }

    public void setMatchList(List<Match> matchList) {
        this.matchList = matchList;
    }

    protected Round(Parcel in) {
        matchCount = in.readString();
        currentMatchday = in.readString();
        name = in.readString();
        seasonId = in.readString();
        roundOrder = in.readString();
        id = in.readString();
        if (in.readByte() == 0x01) {
            matchList = new ArrayList<Match>();
            in.readList(matchList, Match.class.getClassLoader());
        } else {
            matchList = null;
        }
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(matchCount);
        dest.writeString(currentMatchday);
        dest.writeString(name);
        dest.writeString(seasonId);
        dest.writeString(roundOrder);
        dest.writeString(id);
        if (matchList == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeList(matchList);
        }
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<Round> CREATOR = new Parcelable.Creator<Round>() {
        @Override
        public Round createFromParcel(Parcel in) {
            return new Round(in);
        }

        @Override
        public Round[] newArray(int size) {
            return new Round[size];
        }
    };
}