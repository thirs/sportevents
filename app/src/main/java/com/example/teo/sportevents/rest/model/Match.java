package com.example.teo.sportevents.rest.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Teo on 5.7.2016..
 */
public class Match implements Parcelable {
    @SerializedName("live_status")
    private String liveStatus = null;
    @SerializedName("current_period")
    private String currentPeriod = null;
    @SerializedName("current_minute")
    private String currentMinute = null;
    @SerializedName("attendance")
    private String attendance = null;
    @SerializedName("current_period_start")
    private String currentPeriodStart = null;
    @SerializedName("group_matchday")
    private String groupMatchday = null;
    @SerializedName("away")
    private Away away = null;
    @SerializedName("matchday")
    private String matchday = null;
    @SerializedName("venue")
    private Venue venue = null;
    @SerializedName("match_result")
    private List<MatchRes> matchResList = null;
    @SerializedName("match_date")
    private String matchDate = null;
    @SerializedName("finished")
    private String finished = null;
    @SerializedName("match_time")
    private String matchTime = null;
    @SerializedName("match_meta")
    private List<MatchMeta> matchMetaList = null;
    @SerializedName("winner_team_id")
    private String winnerTeamId = null;
    @SerializedName("home")
    private Home home = null;
    @SerializedName("referee")
    private Referee referee = null;
    @SerializedName("id")
    private String id = null;

    public String getLiveStatus() {
        return liveStatus;
    }

    public void setLiveStatus(String liveStatus) {
        this.liveStatus = liveStatus;
    }

    public String getCurrentPeriod() {
        return currentPeriod;
    }

    public void setCurrentPeriod(String currentPeriod) {
        this.currentPeriod = currentPeriod;
    }

    public String getCurrentMinute() {
        return currentMinute;
    }

    public void setCurrentMinute(String currentMinute) {
        this.currentMinute = currentMinute;
    }

    public String getAttendance() {
        return attendance;
    }

    public void setAttendance(String attendance) {
        this.attendance = attendance;
    }

    public String getCurrentPeriodStart() {
        return currentPeriodStart;
    }

    public void setCurrentPeriodStart(String currentPeriodStart) {
        this.currentPeriodStart = currentPeriodStart;
    }

    public String getGroupMatchday() {
        return groupMatchday;
    }

    public void setGroupMatchday(String groupMatchday) {
        this.groupMatchday = groupMatchday;
    }

    public Away getAway() {
        return away;
    }

    public void setAway(Away away) {
        this.away = away;
    }

    public String getMatchday() {
        return matchday;
    }

    public void setMatchday(String matchday) {
        this.matchday = matchday;
    }

    public Venue getVenue() {
        return venue;
    }

    public void setVenue(Venue venue) {
        this.venue = venue;
    }

    public List<MatchRes> getMatchResList() {
        return matchResList;
    }

    public void setMatchResList(List<MatchRes> matchResList) {
        this.matchResList = matchResList;
    }

    public String getMatchDate() {
        return matchDate;
    }

    public void setMatchDate(String matchDate) {
        this.matchDate = matchDate;
    }

    public String getFinished() {
        return finished;
    }

    public void setFinished(String finished) {
        this.finished = finished;
    }

    public String getMatchTime() {
        return matchTime;
    }

    public void setMatchTime(String matchTime) {
        this.matchTime = matchTime;
    }

    public List<MatchMeta> getMatchMetaList() {
        return matchMetaList;
    }

    public void setMatchMetaList(List<MatchMeta> matchMetaList) {
        this.matchMetaList = matchMetaList;
    }

    public String getWinnerTeamId() {
        return winnerTeamId;
    }

    public void setWinnerTeamId(String winnerTeamId) {
        this.winnerTeamId = winnerTeamId;
    }

    public Home getHome() {
        return home;
    }

    public void setHome(Home home) {
        this.home = home;
    }

    public Referee getReferee() {
        return referee;
    }

    public void setReferee(Referee referee) {
        this.referee = referee;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    protected Match(Parcel in) {
        liveStatus = in.readString();
        currentPeriod = in.readString();
        currentMinute = in.readString();
        attendance = in.readString();
        currentPeriodStart = in.readString();
        groupMatchday = in.readString();
        away = (Away) in.readValue(Away.class.getClassLoader());
        matchday = in.readString();
        venue = (Venue) in.readValue(Venue.class.getClassLoader());
        if (in.readByte() == 0x01) {
            matchResList = new ArrayList<MatchRes>();
            in.readList(matchResList, MatchRes.class.getClassLoader());
        } else {
            matchResList = null;
        }
        matchDate = in.readString();
        finished = in.readString();
        matchTime = in.readString();
        if (in.readByte() == 0x01) {
            matchMetaList = new ArrayList<MatchMeta>();
            in.readList(matchMetaList, MatchMeta.class.getClassLoader());
        } else {
            matchMetaList = null;
        }
        winnerTeamId = in.readString();
        home = (Home) in.readValue(Home.class.getClassLoader());
        referee = (Referee) in.readValue(Referee.class.getClassLoader());
        id = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(liveStatus);
        dest.writeString(currentPeriod);
        dest.writeString(currentMinute);
        dest.writeString(attendance);
        dest.writeString(currentPeriodStart);
        dest.writeString(groupMatchday);
        dest.writeValue(away);
        dest.writeString(matchday);
        dest.writeValue(venue);
        if (matchResList == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeList(matchResList);
        }
        dest.writeString(matchDate);
        dest.writeString(finished);
        dest.writeString(matchTime);
        if (matchMetaList == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeList(matchMetaList);
        }
        dest.writeString(winnerTeamId);
        dest.writeValue(home);
        dest.writeValue(referee);
        dest.writeString(id);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<Match> CREATOR = new Parcelable.Creator<Match>() {
        @Override
        public Match createFromParcel(Parcel in) {
            return new Match(in);
        }

        @Override
        public Match[] newArray(int size) {
            return new Match[size];
        }
    };
}
