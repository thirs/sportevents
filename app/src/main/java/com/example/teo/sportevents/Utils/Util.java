package com.example.teo.sportevents.Utils;

import android.net.ParseException;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by Teo on 6.7.2016..
 */
public class Util {
    public static String getCurrentDate(){
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date date = new Date(System.currentTimeMillis());
        return dateFormat.format(date);
    }

    public static String reformatPickedDate(int year, int monthOfYear, int dayOfMonth){
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Calendar cal = Calendar.getInstance();
        cal.set(year, monthOfYear, dayOfMonth);
        Date date = cal.getTime();
        return dateFormat.format(date);
    }

    public static Integer[] parseDate(String date){
        Integer[] dateParts = null;
        if(date != null){
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
            Date d = null;
            try {
                d = formatter.parse(date);
            } catch (java.text.ParseException e) {
                e.printStackTrace();
            }
            if(d != null){
                Calendar c = Calendar.getInstance();
                c.setTime(d);
                dateParts = new Integer[3];
                dateParts[0] = c.get(Calendar.YEAR);
                dateParts[1] = c.get(Calendar.MONTH);
                dateParts[2] = c.get(Calendar.DAY_OF_MONTH);
            }
        }

        return dateParts;
    }
}
