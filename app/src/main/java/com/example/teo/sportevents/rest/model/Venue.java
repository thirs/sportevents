package com.example.teo.sportevents.rest.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Teo on 5.7.2016..
 */
public class Venue implements Parcelable {
    @SerializedName("town")
    private Town town = null;
    @SerializedName("kind")
    private String kind = null;
    @SerializedName("name")
    private String name = null;
    @SerializedName("country")
    private Country country = null;
    @SerializedName("venue_detail")
    private VenueDetails venueDetails = null;
    @SerializedName("id")
    private String id = null;

    public Town getTown() {
        return town;
    }

    public void setTown(Town town) {
        this.town = town;
    }

    public String getKind() {
        return kind;
    }

    public void setKind(String kind) {
        this.kind = kind;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Country getCountry() {
        return country;
    }

    public void setCountry(Country country) {
        this.country = country;
    }

    public VenueDetails getVenueDetails() {
        return venueDetails;
    }

    public void setVenueDetails(VenueDetails venueDetails) {
        this.venueDetails = venueDetails;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    protected Venue(Parcel in) {
        town = (Town) in.readValue(Town.class.getClassLoader());
        kind = in.readString();
        name = in.readString();
        country = (Country) in.readValue(Country.class.getClassLoader());
        venueDetails = (VenueDetails) in.readValue(VenueDetails.class.getClassLoader());
        id = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(town);
        dest.writeString(kind);
        dest.writeString(name);
        dest.writeValue(country);
        dest.writeValue(venueDetails);
        dest.writeString(id);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<Venue> CREATOR = new Parcelable.Creator<Venue>() {
        @Override
        public Venue createFromParcel(Parcel in) {
            return new Venue(in);
        }

        @Override
        public Venue[] newArray(int size) {
            return new Venue[size];
        }
    };
}