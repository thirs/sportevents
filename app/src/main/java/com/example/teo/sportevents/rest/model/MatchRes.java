package com.example.teo.sportevents.rest.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Teo on 5.7.2016..
 */
public class MatchRes implements Parcelable {
    @SerializedName("comment")
    private String comment = null;
    @SerializedName("rank")
    private String rank = null;
    @SerializedName("rounds")
    private String rounds = null;
    @SerializedName("team_id")
    private String teamId = null;
    @SerializedName("points")
    private String points = null;
    @SerializedName("place")
    private String place = null;
    @SerializedName("person_id")
    private String personId = null;
    @SerializedName("match_result_at")
    private String matchResultAt = null;
    @SerializedName("match_result")
    private String matchResult = null;

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getRank() {
        return rank;
    }

    public void setRank(String rank) {
        this.rank = rank;
    }

    public String getRounds() {
        return rounds;
    }

    public void setRounds(String rounds) {
        this.rounds = rounds;
    }

    public String getTeamId() {
        return teamId;
    }

    public void setTeamId(String teamId) {
        this.teamId = teamId;
    }

    public String getPoints() {
        return points;
    }

    public void setPoints(String points) {
        this.points = points;
    }

    public String getPlace() {
        return place;
    }

    public void setPlace(String place) {
        this.place = place;
    }

    public String getPersonId() {
        return personId;
    }

    public void setPersonId(String personId) {
        this.personId = personId;
    }

    public String getMatchResultAt() {
        return matchResultAt;
    }

    public void setMatchResultAt(String matchResultAt) {
        this.matchResultAt = matchResultAt;
    }

    public String getMatchResult() {
        return matchResult;
    }

    public void setMatchResult(String matchResult) {
        this.matchResult = matchResult;
    }

    protected MatchRes(Parcel in) {
        comment = in.readString();
        rank = in.readString();
        rounds = in.readString();
        teamId = in.readString();
        points = in.readString();
        place = in.readString();
        personId = in.readString();
        matchResultAt = in.readString();
        matchResult = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(comment);
        dest.writeString(rank);
        dest.writeString(rounds);
        dest.writeString(teamId);
        dest.writeString(points);
        dest.writeString(place);
        dest.writeString(personId);
        dest.writeString(matchResultAt);
        dest.writeString(matchResult);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<MatchRes> CREATOR = new Parcelable.Creator<MatchRes>() {
        @Override
        public MatchRes createFromParcel(Parcel in) {
            return new MatchRes(in);
        }

        @Override
        public MatchRes[] newArray(int size) {
            return new MatchRes[size];
        }
    };
}