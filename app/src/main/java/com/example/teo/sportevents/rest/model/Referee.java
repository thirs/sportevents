package com.example.teo.sportevents.rest.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Teo on 5.7.2016..
 */
public class Referee implements Parcelable {

    @SerializedName("surname")
    private String surname = null;
    @SerializedName("name")
    private String name = null;
    @SerializedName("firstname")
    private String firstname = null;
    @SerializedName("gender")
    private String gender = null;
    @SerializedName("weight")
    private String weight = null;
    @SerializedName("height")
    private String height = null;
    @SerializedName("birthday")
    private String birthday = null;
    @SerializedName("fullname")
    private String fullname = null;
    @SerializedName("id")
    private String id = null;

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getWeight() {
        return weight;
    }

    public void setWeight(String weight) {
        this.weight = weight;
    }

    public String getHeight() {
        return height;
    }

    public void setHeight(String height) {
        this.height = height;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    protected Referee(Parcel in) {
        surname = in.readString();
        name = in.readString();
        firstname = in.readString();
        gender = in.readString();
        weight = in.readString();
        height = in.readString();
        birthday = in.readString();
        fullname = in.readString();
        id = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(surname);
        dest.writeString(name);
        dest.writeString(firstname);
        dest.writeString(gender);
        dest.writeString(weight);
        dest.writeString(height);
        dest.writeString(birthday);
        dest.writeString(fullname);
        dest.writeString(id);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<Referee> CREATOR = new Parcelable.Creator<Referee>() {
        @Override
        public Referee createFromParcel(Parcel in) {
            return new Referee(in);
        }

        @Override
        public Referee[] newArray(int size) {
            return new Referee[size];
        }
    };
}
