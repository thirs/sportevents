package com.example.teo.sportevents.rest.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Teo on 5.7.2016..
 */
public class VenueDetails implements Parcelable {
    @SerializedName("width")
    private String width = null;
    @SerializedName("length")
    private String length = null;
    @SerializedName("capacity")
    private String capacity = null;
    @SerializedName("id")
    private String id = null;
    @SerializedName("built")
    private String built = null;

    public String getWidth() {
        return width;
    }

    public void setWidth(String width) {
        this.width = width;
    }

    public String getLength() {
        return length;
    }

    public void setLength(String length) {
        this.length = length;
    }

    public String getCapacity() {
        return capacity;
    }

    public void setCapacity(String capacity) {
        this.capacity = capacity;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getBuilt() {
        return built;
    }

    public void setBuilt(String built) {
        this.built = built;
    }

    protected VenueDetails(Parcel in) {
        width = in.readString();
        length = in.readString();
        capacity = in.readString();
        id = in.readString();
        built = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(width);
        dest.writeString(length);
        dest.writeString(capacity);
        dest.writeString(id);
        dest.writeString(built);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<VenueDetails> CREATOR = new Parcelable.Creator<VenueDetails>() {
        @Override
        public VenueDetails createFromParcel(Parcel in) {
            return new VenueDetails(in);
        }

        @Override
        public VenueDetails[] newArray(int size) {
            return new VenueDetails[size];
        }
    };
}